;;===========================================================================
(server-start)

;; UTF-8 as default encoding
(set-language-environment "UTF-8")
;;===========================================================================

;;-----------------------------------------
;;Title format : buffer name @ hostname
;;------------------------------------------
(setq frame-title-format (concat "%b@emacs." system-name))

;;turn off toolbar
(tool-bar-mode 0)
;;turn off menubar
(menu-bar-mode 0)
;;use visual bell
(setq visible-bell 1)


;;===========================================================================
;; host specific config
;;===========================================================================
(setq host-custom-init (expand-file-name(concat "~/.emacs.d/host-specific/" system-name ".el")))
(if (file-exists-p host-custom-init)
    (load-file host-custom-init))

;;-------------------------------------------
;; GUI font
;(use-package doom-themes
;  :ensure t
;  :config
  ;; Global settings (defaults)
;; see screenshots https://github.com/doomemacs/themes/tree/screenshots
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-one t)
;  (load-theme 'doom-dark+ t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)
  ;; or for treemacs users
  ;(setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
  ;(doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  ;(doom-themes-org-config)
;)


(setq neo-window-fixed-size nil)
(when (display-graphic-p)
  ;;run M-x all-the-icons-install-fonts to use icons theme
  ;;(setq neo-theme 'icons)
  (when (not (boundp 'my-font))
    (if *win64*
        (setq my-font "Consolas-10")
      (if *is-a-mac*
          (setq my-font "SF Mono-12")
        (setq my-font "Droid Sans Mono Slashed for Powerline-10")))
      )
  (set-frame-font my-font)
  (set-face-attribute 'default t
                      :font my-font)
  (set-fontset-font "fontset-default" 'gb18030 '("Microsoft YaHei" .
                                                                "unicode-bmp"))
  ;the set-frame-font all above seems to be ignored when using doom theme, we call it again after one second
  (run-at-time "1 sec" nil #'set-frame-font my-font)
)


;(setq org-startup-with-inline-images 't)

;;--------------------------------------------------

;;---------------------------------------------------
;; highlight the matching parens
;;---------------------------------------------------
(show-paren-mode t)
(setq show-paren-style 'parentheses)


;;==========================================================================
;; mess around with elisp
;; all those stolen or my own little functions
;;
(require 'elisp-format)


;;--------------------------------------------------------------------------
;; Jump to the matching item
;;--------------------------------------------------------------------------
(global-set-key [(control ?\% )] 'evil-jump-item)

;;--------------------------------------------------------------------------
;; https://www.emacswiki.org/emacs/CopyWithoutSelection
;;--------------------------------------------------------------------------
(defun get-point (symbol &optional arg)
  "get the point"
  (funcall symbol arg)
  (point))
(defun copy-thing (begin-of-thing end-of-thing &optional arg)
  "copy thing between beg & end into kill ring"
  (save-excursion (let ((beg (get-point begin-of-thing 1))
                        (end (get-point end-of-thing arg)))
                    (copy-region-as-kill beg end))))
(defun paste-to-mark
    (&optional
     arg)
  "Paste things to mark, or to the prompt in shell-mode"
  (let ((pasteMe (lambda()
                   (if (string= "shell-mode" major-mode)
                       (progn (comint-next-prompt 25535)
                              (yank))
                     (progn (goto-char (mark))
                            (yank) )))))
    (if arg (if (= arg 1) nil (funcall pasteMe))
      (funcall pasteMe))))

(defun copy-word
    (&optional
     arg)
  "Copy words at point into kill-ring"
  (interactive "P")
  (copy-thing 'backward-word 'forward-word arg)
  ;;(paste-to-mark arg)
  )

;;==========================================================================
;; MoinMoin mode
;;==========================================================================
(load-file "~/.emacs.d/lisp/screen-lines.el")
(load-file "~/.emacs.d/lisp/moinmoin-mode.el")

;;==========================================================================
;; Python related stuff
;;==========================================================================
(add-to-list 'auto-mode-alist '("\\SConscript$" . python-mode))
(add-to-list 'auto-mode-alist '("\\SConstruct$" . python-mode))
(setq python3 (executable-find "python3"))
(when (not (equal nil python3))
  (setq elpy-rpc-python-command python3)
  (setq elpy-interactive-python-command python3)
  )
(setq python-shell-interpreter "ipython3"
      python-shell-interpreter-args "-i --simple-prompt")

(add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))


;; default stuff that can be overridden by host specific config below
;(when *wsl*
;  (setq deft-directory "/mnt/i/My Drive/org-mode")
;  (setq org-directory deft-directory)
;                                        ;(setq ispell-program-path "C:/Users/murph/scoop/apps/msys2/current/usr/bin")
;  )


;;==========================================================================
;;File/buffer name convention: this-is-a-post.rst
;;==========================================================================
(defun my-blog-rst ()
  (interactive)
  (setq title (replace-regexp-in-string ".rst" "" (replace-regexp-in-string "-" " " (buffer-name))))
  (setq bar (make-string (length title) ?=))
  (insert bar)
  (newline)
  (insert title)
  (newline)
  (insert bar)
  (newline)
  (insert ":category: ")
  (newline)
  (insert ":tags: ")
  (newline)
  (insert ":date: ")
  (insert (format-time-string "%Y-%m-%d %H:%M:%S"))
  (newline)
  (newline))

;;;==========================================================================
;;; org publication
;;;==========================================================================
;(require 'ox-publish)
;;; Setup
;(setq org-base-dir (file-name-directory deft-directory))
;(setq org-pub-dir (concat org-base-dir "html"))
;(setq org-publish-project-alist `(("org-files" :base-directory  ,org-base-dir
;                                   :base-extension "org"
;                                   :recursive t
;                                   :publishing-directory ,org-pub-dir
;                                   :publishing-function org-html-publish-to-html
;                                   :headline-levels 4
;                                   :auto-preamble t
;                                   :html-head-extra
;                                   "<link rel=\"stylesheet\" href=\"https://jgkamat.github.io/src/jgkamat.css\">")
;                                  ("static-files" :base-directory ,org-base-dir
;                                   :base-extension
;                                   "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
;                                   :publishing-directory ,org-pub-dir
;                                   :recursive t
;                                   :publishing-function org-publish-attachment)
;                                  ("org" :components ("org-files" "static-files"))))


;;==========================================================================
;; Emacs version compatability
;;==========================================================================
;(when (or *emacs25*
;          (and *emacs24*
;               (not *emacs24old)))
;  (superword-mode t))

;;===========================================================================
;; Keys mapping
;;===========================================================================

;; magit
(global-set-key (kbd "C-x g") 'magit-status)

;; https://github.com/emacs-helm/helm-ls-git
(global-set-key (kbd "M-p") 'helm-browse-project)

(global-set-key [(control -)] 'set-mark-command)
(global-set-key [f2] 'deft)
(global-set-key [f4] 'ibuffer)
(global-set-key [f5] 'neotree-toggle)
(global-set-key [(meta g)] 'goto-line)


(when *is-a-mac*
  (global-unset-key [home])
  (global-set-key [home] 'move-beginning-of-line)
  (global-unset-key [end])
  (global-set-key [end] 'move-end-of-line)
  )

;;===========================================================================
;;export installed packages
;;
;;https://github.com/redguardtoo/elpa-mirror
;;(add-to-list 'load-path "~/.emacs.d/site-lisp/elpa-mirror")
;;(require 'elpa-mirror)
;;(elpamr-create-mirror-for-installed)
