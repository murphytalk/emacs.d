(with-eval-after-load 'lsp-mode
  (setq lsp-file-watch-ignored-directories
      '(".git" "v"))
  (message "lsp configured")
)

(provide 'my-init-lsp)