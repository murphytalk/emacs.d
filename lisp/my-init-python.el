;; -*- coding: utf-8; lexical-binding: t; -*-
(defun my-python-mode-hook ()
  "Custom Python mode hook"
  (message "Running my Python mode hook")
  (require 'lsp-pyright)
  (lsp-deferred)
  )

(add-hook 'python-mode-hook 'my-python-mode-hook)

(provide 'my-init-python)
